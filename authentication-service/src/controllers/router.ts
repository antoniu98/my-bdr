import { Router } from "express"
import { authRouter } from "./AuthController"

const router: Router = Router()
router.use("/auth", authRouter)

export { router }