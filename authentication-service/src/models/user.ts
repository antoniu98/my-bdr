import mongoose from 'mongoose'

interface User {
    email: String;
    fullName: String;
    password: String;
    role: String;
}

interface UserModelInterface extends User, mongoose.Document {}

const userSchema = new mongoose.Schema({
    email: {
        type: String,
        required: true, 
        unique: true
    },
    fullName: {
        type: String,
        required: true 
    },
    password: {
        type: String,
        required: true
    },
})

export const UserModel = mongoose.model<UserModelInterface>('User', userSchema)