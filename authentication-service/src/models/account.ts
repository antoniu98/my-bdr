import mongoose from 'mongoose'

interface Account {
    email: String,
    RON: Number,
    USD: Number,
    EUR: Number
}

interface AccountModelInterface extends Account, mongoose.Document {}

const accountSchena = new mongoose.Schema({
    email: {
        type: String,
        required: true, 
        unique: true
    },
    RON: {
        type: Number,
    },
    USD: {
        type: Number,
    },
    EUR: {
        type: Number,
    },
})

export const AccountModel = mongoose.model<AccountModelInterface>('Account', accountSchena)