import mongoose from 'mongoose'

interface Transaction {
    emailFrom: String;
    emailTo: String;
    amount: String;
    currency: String;
    date: Date;
}

interface TransactionModelInterface extends Transaction, mongoose.Document {}

const TransactionSchema = new mongoose.Schema({
    emailFrom: {
        type: String,
        required: true, 
    },
    emailTo: {
        type: String,
        required: true, 
    },
    amount: {
        type: String,
        required: true, 
    },
    currency: {
        type: String,
        required: true, 
    },
    date: {
        type: Date,
        required: true, 
    },
})

export const TransactionModel = mongoose.model<TransactionModelInterface>('Transaction', TransactionSchema)