import mongoose from 'mongoose'

interface Account {
    email: String;
    RON: Number;
    USD: Number;
    EUR: Number;
    transactions: Object;
    // fullName: String;
    // password: String;
    // role: String;
}

interface AccountModelInterface extends Account, mongoose.Document {}

const accountSchema = new mongoose.Schema({
    email: {
        type: String,
        required: true, 
        unique: true
    },
    RON: {
        type: Number,
        required: true 
    },
    USD: {
        type: Number,
        required: false
    },
    EUR: {
        type: Number,
        required: false
    },
    transactions: {
        type: Object,
        required: false
    }
})

export const AccountModel = mongoose.model<AccountModelInterface>('Account', accountSchema)