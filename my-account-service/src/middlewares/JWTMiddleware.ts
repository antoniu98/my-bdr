import { NextFunction, Request, Response} from "express";
import { ServerError } from "../models/serverError";

const axios = require("axios")

export const authorizeTokenAndExtractUser = async (req:Request, res:Response, next:NextFunction) => {
    try {
        /* Check token existance */
        if (!req.body.token && !req.headers.token)
            throw new ServerError("Lipseste tokenul pentru autentificare!", 401)

        const token = req.body.token || req.headers.token
        var user:any = null
        var transactions:any = null
        const response = await axios.post("http://localhost:3000/auth/checkjwt", {}, {
                         headers: {token: token}
                        })

        if (!response.data.user) {
            console.error(`[JWT Middleware]: Request to auth service returned with error.`)
            throw new ServerError("No user found after decrypting token error", 403)
        }
        // console.log(token);
        // const token = req.body.token || req.headers.token
        // const response2 = await axios.get("http://localhost:3002/api/payment", {}, {
        //                  headers: {token: token}
        //                 })
        // console.log(token);
        // console.log(response2);
        // if (!response2.data.transactions) {
        //     console.error(`[JWT Middleware]: Request to auth service returned with error. Tura 2`)
        //     throw new ServerError("No transaction found after decrypting token error", 403)
        // }
        // transactions = response2.data.transactions;
        user = response.data.user
        //console.log(`[JWT Middleware] User ${JSON.stringify(response.data)}`) //uncomment if debugging needed
        // Add user details for next middlewares
        req.body.user = user
        // req.body.transactions = transactions
        next()
    } catch (err) {
        next(err)
    }
}

export const authorizeTokenAndExtractTransactions = async (req:Request, res:Response, next:NextFunction) => {
    try {
        /* Check token existance */
        if (!req.body.token && !req.headers.token)
            throw new ServerError("Lipseste tokenul pentru autentificare!", 401)

        const token = req.body.token || req.headers.token
        var user:any = null
        var transactions:any = null
        // const response = await axios.post("http://localhost:3000/auth/checkjwt", {}, {
        //                  headers: {token: token}
        //                 })

        // if (!response.data.user) {
        //     console.error(`[JWT Middleware]: Request to auth service returned with error.`)
        //     throw new ServerError("No user found after decrypting token error", 403)
        // }
        // console.log(token);
        // const token = req.body.token || req.headers.token
        const response = await axios.get("http://localhost:3002/api/payment", {
                         headers: {token: token}
                        })
        console.log(token);
        if (!response.data.transactions) {
            console.error(`[JWT Middleware]: Request to auth service returned with error. Tura 2`)
            throw new ServerError("No transaction found after decrypting token error", 403)
        }
        // console.log(response.data.transactions);
        transactions = response.data.transactions;
        console.log(transactions);
        // user = response.data.user
        //console.log(`[JWT Middleware] User ${JSON.stringify(response.data)}`) //uncomment if debugging needed
        // Add user details for next middlewares
        // req.body.user = user
        // console.log(req.body);
        req.body.transactions = transactions
        next()
    } catch (err) {
        next(err)
    }
}