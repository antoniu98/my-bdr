import { Router } from "express"
import { accountRouter } from "./AccountController"
import { authorizeTokenAndExtractUser, authorizeTokenAndExtractTransactions } from "../middlewares/JWTMiddleware"
const router: Router = Router()
router.use("/account", authorizeTokenAndExtractUser, authorizeTokenAndExtractTransactions, accountRouter)

export { router }