
import { Response, Request, NextFunction, Router } from "express";
import { AccountService } from "../services/AccountService"

console.log("AccountController.ts EXECUTED.")

class AccountController {
    /**
     * * By default, each function from accountService will throw an error, so no 'if' statements needed
     */
    public static accountService: AccountService = new AccountService()

    public static async home(req: Request, res: Response, next: NextFunction) {
        try {
            /* 1. get user by email from db */
           try {
            // let {user} = req.body
            // console.log(user);
           	
            
		        const { user } = req.body
		        var startDate: any = req.query.startDate
                var endDate: any = req.query.endDate
		        console.log(user);
		        let account = await AccountController.accountService.findAccount(user.email)
		        const transactions = req.body.transactions;
		        //await PaymentController.paymentService.getTransactionsByUserId(user.email, startDate, endDate)
		        endDate = Date.now();
		        startDate = new Date("January 27, 2021")
		        startDate.setMonth(startDate.getMonth() - 1);
		       const thisMonthTransactions = await AccountController.accountService.getTransactionsByUserId(user.email, startDate, endDate)
		        res.send({
		        	account: {
	                    id: account._id,
	                    email: account.email,
	                    RON: account.RON,
	                    USD: account.USD,
	                    EUR: account.EUR
	                },
		            transactions: transactions,
		            thisMonthTransactions: thisMonthTransactions
		        })
		    } catch (err) {
		        next(err)
		    }
            // let account = await AccountController.accountService.findAccount(email)

            /* 2. check password: if passw is wrong it will throw an error */
            // await AccountController.accountService.checkPassword(password, user.password)

            // /* 3. generate jwt token */
            // const token = await AccountController.accountService.generateJwtLoginToken({
            //     id: user._id,
            //     email: user.email,
            //     fullName: user.fullName
            // })

            /* 4. return the jwt token */
            // res.send({
            //     // token: token,
            //     account: {
            //         id: account._id,
            //         email: account.email,
            //         RON: account.RON,
            //         USD: account.USD,
            //         EUR: account.EUR
            //     }
            // })
        } catch (err) {
            next(err)
        }
    }

    public static async register(req: Request, res: Response, next: NextFunction) {
        try {
            const {email, fullName, password} = req.body
            /* 1. cripteaza parola */
            const hashedPassword: String = await AccountController.accountService.hashPassword(password)

            /* 2. check user in db: it will throw an error if it does not exist*/
            await AccountController.accountService.checkUserInDB(email)

            /* 3. Add user in db */
            await AccountController.accountService.addUserToDb(email, fullName, hashedPassword)

            /* 4. Send message */
            res.send({
                message: "Inregistrare cu succes."
            })
        } catch (err) {
            next(err)
        }
    }

    public static async checkJWT(req: Request, res: Response, next: NextFunction) {
        try {
            const token = req.headers.token || req.body.token
            console.log(`Token to be checked : ${token}`)
            const user = await AccountController.accountService.checkJWTToken(token)
            res.send({
                user: user
            })
        } catch (err) { 
            next(err) 
        }
    }
}

export const accountRouter: Router = Router()
accountRouter.post('/home', AccountController.home)
// accountRouter.post('/register', AccountController.register)
// accountRouter.post('/checkjwt', AccountController.checkJWT)