import { Response, Request, NextFunction, Router } from "express";
import { AuthService } from "../services/AuthService"

console.log("AuthController.ts EXECUTED.")

class AuthController {
    /**
     * * By default, each function from authService will throw an error, so no 'if' statements needed
     */
    public static authService: AuthService = new AuthService()

    public static async login(req: Request, res: Response, next: NextFunction) {
        try {
            /* 1. get user by email from db */
            let {email, password} = req.body
            let user = await AuthController.authService.findUser(email, password)

            /* 2. check password: if passw is wrong it will throw an error */
            await AuthController.authService.checkPassword(password, user.password)

            /* 3. generate jwt token */
            const token = await AuthController.authService.generateJwtLoginToken({
                id: user._id,
                email: user.email,
                fullName: user.fullName
            })

            /* 4. return the jwt token */
            res.send({
                token: token,
                user: {
                    id: user._id,
                    email: user.email,
                    fullName: user.fullName
                }
            })
        } catch (err) {
            next(err)
        }
    }

    public static async register(req: Request, res: Response, next: NextFunction) {
        try {
            const {email, fullName, password} = req.body
            /* 1. cripteaza parola */
            const hashedPassword: String = await AuthController.authService.hashPassword(password)

            /* 2. check user in db: it will throw an error if it does not exist*/
            await AuthController.authService.checkUserInDB(email)

            /* 3. Add user in db */
            await AuthController.authService.addUserToDb(email, fullName, hashedPassword)

            /* 4. Send message */
            res.send({
                message: "Inregistrare cu succes."
            })
        } catch (err) {
            next(err)
        }
    }

    public static async checkJWT(req: Request, res: Response, next: NextFunction) {
        try {
            const token = req.headers.token || req.body.token
            console.log(`Token to be checked : ${token}`)
            const user = await AuthController.authService.checkJWTToken(token)
            res.send({
                user: user
            })
        } catch (err) { 
            next(err) 
        }
    }
}

export const authRouter: Router = Router()
authRouter.post('/login',    AuthController.login)
authRouter.post('/register', AuthController.register)
authRouter.post('/checkjwt', AuthController.checkJWT)