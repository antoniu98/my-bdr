
import { AccountModel } from "../models/Account"
import { verifyAndDecodeJwtToken, createJwtToken } from "./JWTTokenService"
import { comparePassword, hashPassword } from "./PasswordService"
import {TransactionModel} from "../models/transaction"
import { UserModel } from "../models/User"
import { ServerError } from "../models/serverError"
export class AccountService {
    /* Login */
    public async findAccount(email: String):  Promise<any> {
        /* 1. Get the user from db*/
        const account = await AccountModel.findOne({
            'email': email
        }, 
            (err:any, result:any) => {
                if (err) { throw new ServerError(`error: Nu exista un cont cu aceasta adresa de mail: ${email}`, 401) }
            }
        ).clone()
        return account;
        /* 2. Check password hash */
        // if (!user)
        //     throw new ServerError(`Nu exista un user cu aceasta adresa de mail: ${email}`, 401)
        // const correctPassword: Boolean = await comparePassword(password, user.password)
        // if (!correctPassword)
        //     throw new ServerError(`Parola introdusa este gresita.`, 401)

        // return user
    }

    public async getTransactionsByUserId(email: String, startDate: any, endDate: any):  Promise<any> {
        var dateCondition
        if (!startDate && !endDate)
            dateCondition = null
        else
            dateCondition = {
                date: {
                    ...(startDate && {$gte: new Date(startDate)}),
                    ...(endDate && {$lt: new Date(endDate)})
                }
            }
        const condition = {
            $or: [
                {emailFrom: email},
                {emailTo: email},
            ],
            ...(dateCondition && {...dateCondition})
        }
        console.log(`Conditions: ${JSON.stringify(condition)}`)

        const transactions = await TransactionModel.find(condition)
            .clone()
            .catch( (err) => {
                if (err) { 
                    console.log(`Eroare la verificarea tranzactiilor: ${err} for startdate ${startDate} and enddate ${endDate}.`)
                    throw new ServerError(`Eroare la verificarea tranzactiilor.`, 401) 
                }
            })

        if (!transactions)
            throw new ServerError(`Nu am putut prelua toate tranzactiile pentru userul dat.`, 401)

        return transactions
    }
    
    public async checkPassword(password: String, hashedPassword: String): Promise<void> {
        try {
            const correct: Boolean = await comparePassword(password, hashedPassword)
            if (!correct)
                throw new ServerError("Parola nu este corecta", 401)
        } catch(err) {
            throw new ServerError("Eroare la verificarea parolei.", 401)
        }
    }

    public async generateJwtLoginToken(data: any): Promise<String> {
        try {
            return await createJwtToken(data)
        } catch (err) {
            throw new ServerError("Eroare la generarea tokenului jwt.", 401)
        }
    }

    /* Register */
    public async hashPassword(password: String): Promise<String> {
        try {
            return await hashPassword(password)
        } catch (err) {
            console.log(`\n[hashPassword]: eroare la hashingul parolei: ${err}`)
            throw new ServerError('Eroare. Reincercati inregistrarea', 401)
        }
    }

    public async checkUserInDB(email: String): Promise<Boolean> {
        let user
        try {
            user = await UserModel.findOne({ email: email})
        }
        catch (err) {
            console.log(`\n[addUserToDb]: Eroare la verificarea userului in BD: ${err}`)
            throw new ServerError('Eroare. Reincercati inregistrarea', 401)
        }

        if (user)
            return true
        else return false
    }
    
    public async addUserToDb(email: String, fullName: String, hashedPassword: String): Promise<void> {
        const user = new UserModel({
            email: email,
            fullName: fullName,
            password: hashedPassword,
        })
        try {
            await user.save()
        }
        catch (err) {
            console.log(`\n[addUserToDb]: Eroare la adaugarea userului in BD: ${err}`)
            throw new ServerError('Eroare. Reincercati inregistrarea', 401)
        }
    }

    /* check JWT Token */
    public async checkJWTToken(jwt: String): Promise<any> {
        try {
            return await verifyAndDecodeJwtToken(jwt)
        } catch (err) {
            throw new ServerError("Eroare la generarea tokenului jwt.", 401)
        }
    }
}