import { Router } from "express"
import { authorizeTokenAndExtractUser } from "../middlewares/JWTMiddleware"
import { creditRouter, checkToken } from "./CreditController"

const router: Router = Router()
router.use("/credit", authorizeTokenAndExtractUser, creditRouter)
router.use("/auth", checkToken)

export { router }