import { Response, Request, NextFunction, Router } from "express";
import { CreditService } from "../services/CreditService"

console.log("CreditController.ts EXECUTED.")

class CreditController {
    /**
     * * By default, each function from creditService will throw an error, so no 'if' statements needed
     */
    public static creditService: CreditService = new CreditService()

    public static async checkEligibility(req: Request, res: Response, next: NextFunction) {
        try {
            const {salary, seniority, sum, months} = req.body
            
            /*Check user eligibility for credit*/
            await CreditController.creditService.checkEligibility(salary, seniority, sum, months, res)
        } catch (err) {
            next(err)
        }
    }

    public static async checkJWT(req: Request, res: Response, next: NextFunction) {
        try {
            const token = req.headers.token || req.body.token
            console.log("Test =================")
            console.log(`Token to be checked : ${token}`)
            const user = await CreditController.creditService.checkJWTToken(token)
            res.send({
                user: user
            })
        } catch (err) { 
            next(err) 
        }
    }
}

export const creditRouter: Router = Router()
creditRouter.post('/eligibility', CreditController.checkEligibility)

export const checkToken: Router = Router()
checkToken.post('/checkjwt', CreditController.checkJWT)
