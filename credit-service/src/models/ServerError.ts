export class ServerError extends Error {
    private httpStatusCode: Number 

    constructor(message: string, httpStatus: Number) {
        super()
        this.message = message;
        this.httpStatusCode = httpStatus
        Error.captureStackTrace(this, this.constructor);
    }
}
  