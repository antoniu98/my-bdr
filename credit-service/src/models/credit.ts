import mongoose from 'mongoose'

interface Credit {
    userId: String;
    email: String;
    fullName: String;
    occupation: String;
    salary: String;
    seniority: String;
    sum: String;
    months: String;
}

interface CreditModelInterface extends Credit, mongoose.Document {}

const userSchema = new mongoose.Schema({
    userId: {
        type: String,
        required: true, 
        unique: true
    },
    email: {
        type: String,
        required: true 
    },
    fullName: {
        type: String,
        required: true
    },
    occupation: {
        type: String,
        required: true
    },
    salary: {
        type: Int32Array,
        required: true
    },
    seniority: {
        type: String,
        required: true
    },
    sum: {
        type: String,
        required: true
    },
    months: {
        type: String,
        required: true
    },
})

export const UserModel = mongoose.model<CreditModelInterface>('User', userSchema)