import { ServerError } from "../models/ServerError"
import { UserModel } from "../models/user"
import { verifyAndDecodeJwtToken, createJwtToken } from "./JWTTokenService"
import { comparePassword, hashPassword } from "./PasswordService"
import { Response, Request, NextFunction, Router } from "express";

export class CreditService {
    public async checkEligibility(salary: String, seniority: String, sum: String, months: String, res: Response): Promise<void> {
            var maximumLoan = 4*(Number(salary));
            var sumToReturn = 18/100*Number(sum )+ Number(sum);
            if(Number(seniority) >= 1 && Number(sum) <= maximumLoan) 
                res.send({message: "Imprumutul este realizat pe durata de " + months + " luni. Sunteti eligibil pentru a imprumuta " + sum + " lei si va trebui sa restituiti " + sumToReturn});
            
            if (Number(seniority) < 1 || Number(sum) > maximumLoan)  
               res.send({message: "Userul nu este eligibil pentru credit"});
    }

    /* check JWT Token */
    public async checkJWTToken(jwt: String): Promise<any> {
        try {
            return await verifyAndDecodeJwtToken(jwt)
        } catch (err) {
            throw new ServerError("Eroare la generarea tokenului jwt.", 401)
        }
    }
}