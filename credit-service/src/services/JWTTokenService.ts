import { ServerError } from "../models/ServerError"

//TODO: Make this a class

const jwt = require('jsonwebtoken')
const secret = "change_this_secret"
const options = {
    issuer: "Anto", //process.env.JWT_ISSUER,
    subject: "Verification token", //process.env.JWT_SUBJECT,
    expiresIn: "2h",
}

export async function verifyAndDecodeJwtToken(token: any): Promise<String> {
    try {
        return await jwt.verify(token, secret)
    } catch (err) {
        console.trace(err);
        throw new ServerError(`Eroare la decriptarea tokenului! ${err}`, 401);
    }
}

export async function createJwtToken(data: any): Promise<String> { 
    try {
        return await jwt.sign(data, secret, options)
    } catch (err) {
        console.trace(err);
        throw new ServerError(`Eroare la criptarea tokenului! ${err}`, 401);
    }
}