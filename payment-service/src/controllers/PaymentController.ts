import { Response, Request, NextFunction, Router } from "express";
import { PaymentService as PaymentService } from "../services/PaymentService"


class PaymentController {
    /**
     * * By default, each function from service will throw an error, so no 'if' statements needed
     */
    public static paymentService: PaymentService = new PaymentService()

    public static async getPayments(req: Request, res: Response, next: NextFunction) {
        try {
            const { user } = req.body
            const { startDate, endDate } = req.query
            const transactions = await PaymentController.paymentService.getTransactionsByUserId(user.email, startDate, endDate)

            res.send({
                transactions: transactions
            })
        } catch (err) {
            next(err)
        }
    }

    public static async makePayment(req: Request, res: Response, next: NextFunction) {
        try {
            const { user, amount, currency, emailTo } = req.body
            const response = await PaymentController.paymentService.executeTransaction(user.email, emailTo, amount, currency)

            res.send({
                response: response
            })
        } catch (err) {
            next(err)
        }
    }

    public static async addFunds(req: Request, res: Response, next: NextFunction) {
        try {
            const { user, amount, currency, cardNumber, cardCode, cardOwner } = req.body
            const response = await PaymentController.paymentService.addFunds(user.email, amount, currency, cardNumber, cardCode, cardOwner)

            res.send({
                response: response
            })
        } catch (err) {
            next(err)
        }
    }
}

export const paymentRouter: Router = Router()

paymentRouter.get ("/",     PaymentController.getPayments)
paymentRouter.post("/",     PaymentController.makePayment)
paymentRouter.post("/add",  PaymentController.addFunds)