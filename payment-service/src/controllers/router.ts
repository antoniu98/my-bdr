import { Router } from "express"
import { authorizeTokenAndExtractUser } from "../middlewares/JWTMiddleware"
import { paymentRouter } from "./PaymentController"

const router: Router = Router()
router.use("/payment", authorizeTokenAndExtractUser, paymentRouter)

export { router }