import { NextFunction, Request, Response} from "express";
import { ServerError } from "../models/serverError";

const axios = require("axios")

export const authorizeTokenAndExtractUser = async (req:Request, res:Response, next:NextFunction) => {
    try {
        /* Check token existance */
        if (!req.body.token && !req.headers.token)
            throw new ServerError("Lipseste tokenul pentru autentificare!", 401)

        const token = req.body.token || req.headers.token
        var user:any = null

        const response = await axios.post("http://localhost:3000/api/auth/checkjwt", {}, {
                         headers: {token: token}
                        })

        if (!response.data.user) {
            console.error(`[JWT Middleware]: Request to auth service returned with error.`)
            throw new ServerError("No user found after decrypting token error", 403)
        }
        
        user = response.data.user
        //console.log(`[JWT Middleware] User ${JSON.stringify(response.data)}`) //uncomment if debugging needed
        // Add user details for next middlewares
        req.body.user = user
        next()
    } catch (err) {
        next(err)
    }
}