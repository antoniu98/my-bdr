import { NextFunction, Response, Request } from "express"

export const errorMiddleware = (err: any, req: Request, res: Response, next: NextFunction) => {
    if (err) {
      console.log(`err: ${err}`)
      // res.status(err.httpStatusCode)
      // res.send({err: err.message})
      res.send(`Error: ${err.message}`)
    } else {
      next(err)
    }
}