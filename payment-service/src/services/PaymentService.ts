import { AccountModel } from "../models/account";
import { ServerError } from "../models/serverError";
import { TransactionModel } from "../models/transaction";
import { UserModel } from "../models/user";

export class PaymentService {
    public async getTransactionsByUserId(email: String, startDate: any, endDate: any):  Promise<any> {
        var dateCondition
        if (!startDate && !endDate)
            dateCondition = null
        else
            dateCondition = {
                date: {
                    ...(startDate && {$gte: new Date(startDate)}),
                    ...(endDate && {$lt: new Date(endDate)})
                }
            }
        const condition = {
            $or: [
                {emailFrom: email},
                {emailTo: email},
            ],
            ...(dateCondition && {...dateCondition})
        }
        console.log(`Conditions: ${JSON.stringify(condition)}`)

        const transactions = await TransactionModel.find(condition)
            .clone()
            .catch( (err) => {
                if (err) { 
                    console.log(`Eroare la verificarea tranzactiilor: ${err} for startdate ${startDate} and enddate ${endDate}.`)
                    throw new ServerError(`Eroare la verificarea tranzactiilor.`, 401) 
                }
            })

        if (!transactions)
            throw new ServerError(`Nu am putut prelua toate tranzactiile pentru userul dat.`, 401)

        return transactions
    }
    
    public async addTransaction(emailFrom: String, emailTo: String, amount: String, currency: String) : Promise<any> {
        const transaction = new TransactionModel({
            emailFrom: emailFrom,
            emailTo: emailTo,
            amount: amount,
            currency: currency,
            date: new Date()
        })

        return await transaction.save()
    }

    public async executeTransaction(emailFrom: String, emailTo: String, amount: string, currency: string) : Promise<any> {
        /* Conditions for transaction */
        const userTo = await UserModel.findOne({ email: emailTo })
        if (!userTo)
            throw new ServerError(`Account for email ${emailTo} doesn't exist.`, 401)

        const accountFrom: any  = await AccountModel.findOne({email: emailFrom})
        const accountTo: any    = await AccountModel.findOne({email: emailTo})
        if (parseFloat(accountFrom[currency]) < parseFloat(amount))
            throw new ServerError(`User ${emailFrom} does not have enough money.`, 401)

        /* Transaction possible */
        await this.applyTransaction(accountFrom, accountTo, amount, currency)
    }

    public async applyTransaction(accountFrom: any, accountTo: any, amount: string, currency: string) : Promise<any> {
        /* delete amount from FROM */
        var newAmountFrom = accountFrom[currency] - parseInt(amount)
        const resultFrom = await AccountModel.updateOne(
                { email: accountFrom.email },
                { [currency] : newAmountFrom }
            )
        if (!resultFrom)
            throw new ServerError(`Failed to update account for userFrom.`, 401)


        /* add amount to TO */
        var newAmountTo = accountTo[currency] + parseInt(amount)
        const resultTo = await AccountModel.updateOne(
                { email: accountTo.email },
                { [currency] : newAmountTo }
            )
        if (!resultTo)
            throw new ServerError(`Failed to update account for userTo.`, 401) //TODO cancel previous accountFrom

        /* add transaction to the DB */
        return await this.addTransaction(accountFrom.email, accountTo.email, amount, currency)
    }

    public async addFunds(email: string, amount: any, currency: any, cardNumber: any, cardCode: any, cardOwner: any) : Promise<any> {
        if (!amount || !currency || !cardNumber || !cardCode || !cardOwner)
            throw new ServerError(`One of the following details not provided: amount, currency, card number, card code, card owner.`, 403)

        /* Add money to account */
        const account: any  = await AccountModel.findOne({email: email})
        const newAmount: any = account[currency] + parseFloat(amount)
        const responseAccount = await AccountModel.updateOne(
            { email: email},
            { [currency] : newAmount}
        )

        /* Add transaction */
        const responseTransaction = await this.addTransaction("Added funds", email, amount, currency)

        if (!responseAccount || !responseTransaction)
            throw new ServerError(`Couldn't add money to the ${email}`, 301)
        return "Funds added succesfully."
    }    
}