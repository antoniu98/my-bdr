import { NextFunction, Response, Request } from "express"

export const errorMiddleware = (err: any, req: Request, res: Response, next: NextFunction) => {
    if (err) {
      console.log(`err: ${err}`)
      res.send(`Eroare: ${err.message}`)
    } else {
      next(err)
    }
}