import { NextFunction, Request, Response} from "express";
import { ServerError } from "../models/ServerError";
import { endpoints } from "../config/externalServices";

const axios = require("axios")

export const authorizeTokenAndExtractUser = async (req:Request, res:Response, next:NextFunction) => {
    try {
        /* Check token existance */
        if (!req.body.token && !req.headers.token)
            throw new ServerError("Lipseste tokenul pentru autentificare!", 401)

        const token = req.body.token || req.headers.token
        var user:any = null

        await axios.post(endpoints["auth-decrypt-token"], {}, {
                         headers: {token: token}
                        })
            .then( (response:any) => {
                user = response.data.user
                console.log(`[JWT Middleware] User ${JSON.stringify(user)}`)
                // Add user details for next middlewares
                req.body.user = user
                next()
            }).catch( (err: any) => {
                console.error(`User token decrypting failed with error: ${err}`)
            })

        if (!user) throw new ServerError("No user found after decrypting token error", 403)
    } catch (err) {
        next(err)
    }
}