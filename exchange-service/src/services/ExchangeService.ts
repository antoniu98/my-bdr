import { ServerError } from "../models/ServerError"
// import { AccountDAO } from "../DAO/AccountDAO"
import { AccountModel } from "../models/Account";
import { TransactionModel } from "../models/transaction";
import { UserModel } from "../models/User";

const axios = require("axios")


export class ExchangeService {
    // private accountDAO: AccountDAO = new AccountDAO()

    public async getExchangedAmount(amount: any, from: any, to: any) : Promise<String> {
        var result: any = ""
        await axios.get(`https://freecurrencyapi.net/api/v2/latest?apikey=251a9c80-7c7b-11ec-a02b-a97509f63d44&base_currency=${from}`)
            .then( (response:any) => {
                console.log(`[ExchangeService] Result from the exchange: 1 ${from} = ${JSON.stringify(response.data.data[to])} ${to}`)
                const ratio = response.data.data[to]
                result = amount * ratio
            }).catch( (err: any) => {
                console.log(`[ExchangeService] Error : ${err.message}`)
            })
        if (result == "") throw new ServerError("FreeCurrencyApi error", 403)

        return result
    }

    public async exchange(email: String, amount: any, from: any, to: any) : Promise<String> {
        /* Check if account has the amount to change. If not, throw server error */
        // const account = this.accountDAO.checkAmountForCurrency(userId, amount, from);
        const account = await AccountModel.findOne({ email: email})
        var newAmount:any;
        console.log(account);
        if (account) {
            if (from == "RON") {
                if (account.RON < amount) {
                    throw new ServerError("The wanted amount is too big", 403);
                } else {
                    newAmount = await this.getExchangedAmount(amount, from, to);
                }
            } else {
                if (from == "EUR") {
                    if (account.EUR < amount) {
                        throw new ServerError("The wanted amount is too big", 403);
                    } else {
                        newAmount = await this.getExchangedAmount(amount, from, to);
                    }
                } else {
                    if (from == "USD") {
                        if (account.USD < amount) {
                            throw new ServerError("The wanted amount is too big", 403);
                        } else {
                            newAmount = await this.getExchangedAmount(amount, from, to);
                        }
                    }
                }
            }
        }
        
        return newAmount;
    }

    // public async executeTransaction(emailFrom: String, emailTo: String, amount: string, currency: string) : Promise<any> {
    //     /* Conditions for transaction */
    //     const userTo = await UserModel.findOne({ email: emailTo })
    //     if (!userTo)
    //         throw new ServerError(`Account for email ${emailTo} doesn't exist.`, 401)

    //     const accountFrom: any  = await AccountModel.findOne({email: emailFrom})
    //     const accountTo: any    = await AccountModel.findOne({email: emailTo})
    //     if (parseFloat(accountFrom[currency]) < parseFloat(amount))
    //         throw new ServerError(`User ${emailFrom} does not have enough money.`, 401)

    //     /* Transaction possible */
    //     await this.applyTransaction(accountFrom, accountTo, amount, currency)
    // }

    public async applyTransaction(account: any, amount: string, currency: string, lastCurrency: string) : Promise<any> {
        /* delete amount from FROM */
        // console.log("dni", account.String(currency));
        // console.log(account[currency]);
        // if (currency )
        // console.log(parseInt(amount))
        var newAmountFrom = parseInt(account[currency]) 
        newAmountFrom += parseInt(amount)
        // console.log(newAmountFrom);
        const resultFrom = await AccountModel.updateOne(
                { email: account.email },
                { [currency] : newAmountFrom }
            )
        // console.log(resultFrom);
        if (!resultFrom)
            throw new ServerError(`Failed to update account for userFrom.`, 401)


        // /* add amount to TO */
        // console.log("gigel");
        // console.log(amount);
        // console.log(account[lastCurrency]);
        // console.log(account[currency]);
        const a = await this.getExchangedAmount(amount, currency, lastCurrency);
        // console.log(a);
        var value = parseInt(amount) * parseInt(String(a));
        // console.log(value);
        var newAmountTo = account[lastCurrency] - value;
        // console.log(newAmountTo);
        const resultTo = await AccountModel.updateOne(
                { email: account.email },
                { [lastCurrency] : newAmountTo }
            )
        // console.log(resultTo);
        if (!resultTo)
            throw new ServerError(`Failed to update account for userTo.`, 401) //TODO cancel previous accountFrom
        return resultTo;
        /* add transaction to the DB */
        // return await this.addTransaction(accountFrom.email, accountTo.email, amount, currency)
    }

    public async addTransaction(emailFrom: String, emailTo: String, amount: String, currency: String) : Promise<any> {
        const transaction = new TransactionModel({
            emailFrom: emailFrom,
            emailTo: emailTo,
            amount: amount,
            currency: currency,
            date: new Date()
        })

        return await transaction.save()
    }
}