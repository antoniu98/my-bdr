import { Response, Request, NextFunction, Router } from "express"
import { ServerError } from "../models/ServerError"
import { ExchangeService } from "../services/ExchangeService"
import { AccountModel } from "../models/Account";
export const exchangeRouter: Router = Router()

class ExchangeController {
    private static exchangeService: ExchangeService = new ExchangeService()
    private static acceptedCurrencies = ["RON", "USD", "EUR"]
    
    public static async exchangeAmount(req: Request, res: Response, next: NextFunction) {
        try {
            const {to, from, amount} = req.body;
            console.log(to);
            console.log(from);
            console.log(amount);
            /* Params check */
            if (!to || !from || !amount) {
                res.send({
                    error: "Not all params provided"
                })
                throw new ServerError("Not all params provided", 401)
            }
            //TODO separate checkers for params
            if (ExchangeController.acceptedCurrencies.indexOf(to.toString()) < 0 &&
                ExchangeController.acceptedCurrencies.indexOf(from.toString()) < 0) {
                    res.send({
                        error: "Currencies accepted: RON USD EUR"
                    })
                    throw new ServerError("Currencies accepted: RON USD EUR", 401)
                }

            // convert the amount */
            const user = req.body.user
            const account = await AccountModel.findOne({ email: user.email})
            const new_amount = await ExchangeController.exchangeService.exchange(user.email, amount, from, to)
            console.log(new_amount);
            /* 2. update user's account balances */

            /* Send converted amount */
            res.send({
                to: to,
                amount: new_amount
            })
            
        } catch (err) {
            next(err)
        }
    }
    public static async applyExchangeAmount(req: Request, res: Response, next: NextFunction) {
        try {
            const {from, to, amount} = req.body;
            const user = req.body.user;
            console.log(amount);
            console.log(to);
            console.log(user.email);
            const account = await AccountModel.findOne({ email: user.email});
            const final = await ExchangeController.exchangeService.applyTransaction(account, String(amount), to, from);
            // const account = await AccountModel.findOne({ email: user.email})
            if (account){
                console.log(account.email);
            }
            res.send({
                account: account
            })
        }
        catch (err) {
            next(err)
        }   
    }
}

exchangeRouter.post("/asd", ExchangeController.exchangeAmount)
exchangeRouter.post("/applyT", ExchangeController.applyExchangeAmount)