import { Router } from "express"
import { authorizeTokenAndExtractUser } from "../middlewares/JWTMiddleware"
import { exchangeRouter } from "./ExchangeController"

export const router: Router = Router()

router.use('/exchange', authorizeTokenAndExtractUser, exchangeRouter)