import express, {Application} from 'express';
import morgan from "morgan"
import helmet from 'helmet'
import { router } from './controllers/router';
import { errorMiddleware } from './middlewares/errorMiddleware';
import { connectToDatabase } from './config/databaseInit';

const app: Application = express();
const port = 3005;

/* Middlewares */
app.use(helmet())
app.use(morgan(':remote-addr - :remote-user [:date[web]] ":method :url HTTP/:http-version" :status :res[content-length]')); 
app.use(express.json())
app.use('/api', router)
app.use(errorMiddleware)

/* Init DB connection */
connectToDatabase()

app.listen(port, () => {
  return console.log(`Express is listening at http://localhost:${port}`);
});