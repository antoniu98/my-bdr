import mongoose from 'mongoose'

interface Account {
    userId: String,
    email: String,
    RON: String,
    USD: String,
    EUR: String
}

interface AccountModelInterface extends Account, mongoose.Document {}

const accountSchena = new mongoose.Schema({
    userId: {
        type: String,
        required: true, 
        unique: true
    },
    email: {
        type: String,
        required: true
    },
    RON: {
        type: String,
    },
    USD: {
        type: String,
    },
    EUR: {
        type: String,
    },
})

export const AccountModel = mongoose.model<AccountModelInterface>('Account', accountSchena)