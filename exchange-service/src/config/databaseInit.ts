import mongoose, {Connection} from 'mongoose'

// TODO : put password in env
const password = 'admin'
const url = `mongodb+srv://admin:${password}@cluster0.145lp.mongodb.net/my-bdr?retryWrites=true&w=majority`

export function connectToDatabase(): void {
    mongoose
        .connect(url)
        .catch((err: any) => console.log(err.reason));

    const db: Connection = mongoose.connection;

    db.on('error', console.error.bind(console, 'Connection error to MongoDB'));
    db.once('open', () => {
        console.log('Connected to MongoDB');
    });
}